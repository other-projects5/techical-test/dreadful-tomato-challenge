
import { IAppState } from './IAppState';


const state: IAppState = {
	appData: {
		contentList: {
			list: [],
			itemsPerRow: 5,
			skip: 1
		},
		filter: {
			text: '',
			year: 0
		}
	}
};

export const initialState = process.env.NODE_ENV === 'development' ? {
    ...state
	// Here you can set some data by default to got faster developing the GUI
} : state;
