
import { ThunkAction } from 'redux-thunk';
import { AnyAction } from 'redux';


export type Action = ThunkAction<Promise<void> | void, Record<string, unknown>, Record<string, unknown>, AnyAction>;
// - If you want a type meaning "any object", you probably want `Record<string, unknown>` instead.
// - If you want a type meaning "any value", you probably want `unknown` instead.
// - If you want a type meaning "empty object", you probably want `Record<string, never>` instead.
