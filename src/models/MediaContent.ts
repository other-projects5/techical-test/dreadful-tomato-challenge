
export declare type MediaContentType = 'series' | 'movie';

export interface MediaContent {
	title: string;
	description: string;
	type: MediaContentType;
	image: Image;
	releaseYear: number;
}

interface Image {
	url: string,
	metadata: ImageMetadata
}

interface ImageMetadata {
	width: number;
	height: number;
}
