
import * as React from 'react';
import { FunctionComponent, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { IAppState } from '../../../store/IAppState';

import { MediaContent } from '../../../models';
import { Card } from './Card';
import { Steps } from './Steps';

import styles from './GridViewContent.module.scss';


interface GridViewContentProps {
	title?: string;
	rowsAmount?: number;
}

export const GridViewContent: FunctionComponent<GridViewContentProps> = (props) => {

	const { title, rowsAmount } = props;
	const [currentList, setCurrentList] = useState<MediaContent[][]>([]);

	const contentListSelector = useSelector((state:IAppState) => state.appData.contentList);
	const { list, skip, itemsPerRow } = contentListSelector;

	useEffect(() => {
		if (list.length === 0) {
			setCurrentList([]);
			return;
		}

		const grid: MediaContent[][] = [];

		if (rowsAmount) {
			const row = list.slice((skip - 1) * (itemsPerRow * rowsAmount), itemsPerRow * rowsAmount * skip);

			for (let i = 0; i < rowsAmount; i++) {
				grid[i] = row.slice(itemsPerRow * i, itemsPerRow + itemsPerRow * i);
			}

			setCurrentList(grid);
			return;
		}

		const rowsAmountWithoutSteps = Math.floor(list.length / itemsPerRow);

		for (let i = 0; i <= rowsAmountWithoutSteps; i++) {
			grid[i] = list.slice(itemsPerRow * i, itemsPerRow + itemsPerRow * i);
		}

		setCurrentList(grid);
	}, [list, skip, rowsAmount]);

	return (
		<div className={styles.gridWrapper} >
			{ title && (
				<div className={styles.title} >{ title }</div>
			)}

			<div className={styles.listWrapper} >
				{currentList.length === 0 && (
					<div className={styles.noMatch}>
						Any match with this filters
					</div>
				)}

				{ currentList.length > 0 && currentList.map((rows, index) => {
					return (
						<div className={styles.listRow} key={index} >
							{ rows.map((item, index) => (
								<Card key={item.title + '_' + index}
									  urlImage={item.image.url}
									  title={item.title}
									  releaseYear={item.releaseYear}
									  description={item.description} />
							))}
						</div>
					);
				})}

				{ rowsAmount && (
					<Steps rowsAmount={rowsAmount} />
				)}
			</div>
		</div>
	);
};
