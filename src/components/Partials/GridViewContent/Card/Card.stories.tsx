
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { Card } from './Card';


export default {
	title: 'Partials/GridViewContent/Card',
	component: Card,
	args: {
		urlImage: 'https://streamcoimg-a.akamaihd.net/000/117/25/11725-PosterArt-deecf8dbd786dfa2d964413b0bf83726.jpg',
		title: 'Billions'
	},
	decorators: [
		(Card) => (
			<MemoryRouter>
				<Card />
			</MemoryRouter>
		)]
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args) => (<Card {...args} />);

export const Primary = Template.bind({
	urlImage: 'https://streamcoimg-a.akamaihd.net/000/117/25/11725-PosterArt-deecf8dbd786dfa2d964413b0bf83726.jpg',
	title: 'Billions'
});
