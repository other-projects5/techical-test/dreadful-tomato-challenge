
import * as React from 'react';
import { Fragment, FunctionComponent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';

import classNames from 'classnames';
import { Container } from '../../../__lib__/react-components';

import { IAppState } from '../../../store/IAppState';
import { Dispatch } from '../../../store/types/redux';
import { setFilterText } from '../../../store/actions/appDataActions';

import { routes } from '../../routes/config/routes';
import LogoImg from '../../../assets/images/logo.png';

import styles from './Header.module.scss';
import { MediaContentType } from '../../../models';


export const Header: FunctionComponent = () => {

	const { pathname } = useLocation();

	const { text } = useSelector((state: IAppState) => state.appData.filter);
	const dispatch: Dispatch = useDispatch();
	const setFilterTextDispatch = (type: MediaContentType, state: string) => dispatch(setFilterText(type, state));

	const [activeFilter, setActiveFilter] = useState<boolean>(false);

	useEffect(() => {
		setActiveFilter(false);
		onFilterTextChange('');
	}, [pathname]);

	const onFilterTextChange = (text: string) => {
		const type = pathname === routes.movies ? 'movie' : 'series';
		setFilterTextDispatch(type, text);
	};

	return (
		<div className={styles.headerWrapper}>
			<div className={styles.header} >

				<Container style={{ display: 'flex', justifyContent: 'space-between', height: '100%' }} >
					<div className={styles.leftSide} >
						<Link to={routes.home} >
							<img className={styles.logo} src={LogoImg} alt="Logo - Dreadful Tomato" />
						</Link>
						<div className={classNames({ [styles.navItem]: true, [styles.active]: pathname === routes.movies })} >
							<Link to={routes.movies} className={styles.link} >
								Movies
							</Link>
						</div>
						<div className={classNames({ [styles.navItem]: true, [styles.active]: pathname === routes.series })} >
							<Link to={routes.series} className={styles.link} >
								Series
							</Link>
						</div>
					</div>

					<div className={styles.rightSide} >
						{pathname !== routes.home && (
							<Fragment>
								<div className={styles.separator} />
								<div className={classNames({ [styles.navItem]: true, [styles.active]: activeFilter })}
									 onClick={() => setActiveFilter(!activeFilter)} >Filters</div>
							</Fragment>
						)}
						<div className={styles.separator} />
						<div className={styles.navItem} >Login</div>
						<div className={styles.separator} />
						<div className={styles.freeBtn} >Start your free trial</div>
					</div>
				</Container>

			</div>

			<div className={classNames({ [styles.filtersWrapper]: true, [styles.active]: activeFilter })} >
				<Container style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', height: '100%' }} >
					<input className={styles.inputText}
						   type="text"
						   placeholder="Search by title..."
						   value={text}
						   onChange={(event) => onFilterTextChange(event.currentTarget.value)} />
					<div>
						Calendar
					</div>
				</Container>
			</div>
		</div>
	);
};
