
import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { HomePage } from './HomePage';

export default {
	title: 'Pages/Home',
	component: HomePage
} as ComponentMeta<typeof HomePage>;

const Template: ComponentStory<typeof HomePage> = (args) => (<HomePage {...args} />);

export const Primary = Template.bind({});
