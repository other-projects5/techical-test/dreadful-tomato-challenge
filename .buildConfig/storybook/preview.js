export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  backgrounds: {
    default: 'DreadfulTomato',
    values: [{
      name: 'DreadfulTomato',
      value: '#222'
    }]
  }
}
